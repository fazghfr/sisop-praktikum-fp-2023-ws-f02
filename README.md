# sisop-praktikum-fp-2023-WS-F02

Kelas Sistem Operasi F - Kelompok F02

## Anggota Kelompok :
- Salsabila Fatma Aripa (5025211057)
- Arfi Raushani Fikra (5025211084)
- Ahmad Fauzan Alghifari (5025211091)

### Autentikasi
Untuk implementasi autentikasi kelompok kami, sesuai dengan apa yang diinstruksikan pada soal, terdapat dua mode, yaitu mode non-root dan mode root.

Potongan kode dibawah bertujuan untuk mengecek user tersebut, apakah root atau tidak.
```c
    int is_sudo = (geteuid() == 0);


    char* username = NULL;
    char* password = NULL;

    // Check if running as sudo or non-sudo
    if (is_sudo) {
        
        if (argc != 1) {
            print_usage();
            return 1;
        }

        
    } else {
        // Running as non-sudo, login with username and password
        int option;

        // Parse command line arguments
        while ((option = getopt(argc, argv, "u:p:")) != -1) {
            switch (option) {
                case 'u':
                    username = optarg;
                    break;
                case 'p':
                    password = optarg;
                    break;
                default:
                    print_usage();
                    return 1;
            }
        }

        // Validate command line arguments
        if (username == NULL || password == NULL || strcmp(username, "admin")==0) {
            print_usage();
            return 1;
        }
    }
```

Untuk mode non-root, akan langsung diberikan request di server untuk username dan passwordnya.
Kemudian nantinya, server akan mengirimkan response. Jika response sesuai, akan masuk ke fungsi login_main()
```c
 if (!is_sudo) {
        sprintf(request, "LOGIN USER %s IDENTIFIED BY %s; [%s]", username, password, username);

            // Send the request to the server
        if (send(client_socket, request, strlen(request), 0) == -1) {
            perror("Send failed");
            return 1;
        }

        // Receive and print the response from the server
        if (recv(client_socket, response, sizeof(response), 0) == -1) {
            perror("Receive failed");
            return 1;
        }
        printf("=> [Server Response] : %s\n", response);
    } 

    
    if(strstr(response, "LOGGED IN")!=NULL && !is_sudo){
        client_socket = connect_me();
        login_main(client_socket, username, is_sudo);
    } else if (is_sudo){
        login_main(client_socket, "admin", is_sudo);
    }
```

Berikut adalah fungsi login_main(), dimana ini bertindak sebagai "main" setelah login berhasil.
```c
void login_main(int client_socket, char *clientname, int is_sudo) {
    char cmd[1024];
    char response[1024];

    char cur_database[1024];

    int i = 0;
    do {
        if(i!=0){
            client_socket = connect_me();
        }
        fgets(cmd, sizeof(cmd), stdin);
        cmd[strcspn(cmd, "\n")] = '\0'; 

        if (strcmp(cmd, "\n") == 0){
            continue;
        }

        char *request = malloc(strlen(clientname) + 1024);
        strcpy(request, cmd);
        strcat(request, " [");
        strcat(request, clientname);
        strcat(request, "]");

        if(strlen(cur_database)!=0){
            char tempstr[256];
            sprintf(tempstr, "%s", cur_database);
            strcat(request, tempstr);
        }

        // request format: [COMMAND] -u [CLIENTNAME]
        if (send(client_socket, request, strlen(request), 0) == -1) {
            perror("Send failed");
            break;
        }

        if (recv(client_socket, response, sizeof(response), 0) == -1) {
            perror("Receive failed");
            return;
        }

        printf("=> [Server Response] : %s\n", response);
        if(strcmp(response, "Client Session closed")==0){
            break;
        } else if(strstr(response, "Database Used")!=0){
            extractDatabaseName(response, cur_database);
        }
        free(request);
        i++;
    } while (1);
}
```
### Autorisasi
Terkait autorisasi, seorang pengguna yang bukan pemilik (pencipta) suatu database tidak dapat menggunakan database tersebut. Maka dari itu, dengan bantuan admin, pengguna tersebut dapat diberikan hak akses untuk database tertentu yang bukan miliknya. Perintah pemberian izin atau hak akses ini, dituliskan melalui program client, yang setelah itu akan dieksekusi oleh server. Berikut potongan kode yang berada pada file program database
```c
        else if(strstr(request, "GRANT PERMISSION")!=NULL){
            int is_admin = strcmp(sender, "admin")==0;

            if(is_admin){
                char dbname[1024];
                char granted_user[1024];
                auth_getdb(request, granted_user, dbname);
                printf("nama %s db %s\n", granted_user, dbname);

                //cek db's existance
                if(check_db(dbname)!=1){
                    printf("DB: %s does not exist\n", dbname);
                    sprintf(response, "%s", "Database Does not exist");
                }

                //check user existance
                else if(check_username(granted_user)){
                    int state = grant_userdb(granted_user, dbname, 0);
                    if(state!=0){
                        if(state == -2){
                            sprintf(response, "%s", "User Already Granted on Database");
                        }else{
                            sprintf(response, "%s", "Failed to grant user");
                        }
                    }else{
                        sprintf(response, "%s", "Successfully Granted the User");
                    }
                } else {
                    sprintf(response, "%s %s", granted_user, "is unregistered as a user");
                }                
            } else{
                sprintf(response, "%s", "Non-admin can not grant other user's permission");
            }
        }
```
Pada kode di atas, terdapat percabangan yang melakukan pemeriksaan terhadap *request* oleh pengguna (admin), yaitu perintah `GRANT PERMISSION`. Dengan perintah ini, maka admin dapat memberikan hak akses terhadap suatu database kepada seorang pengguna. Potongan kode tersebut dibantu beberapa fungsi seperti `auth_getdb` yang digunakan untuk mengambil nama database dan nama pengguna dari perintah `GRANT PERMISSION`, lalu ada fungsi `check_db` yang digunakan untuk memeriksa apakah database yang dimaksud tersedia atau tidak, selain itu terdapat fungsi `check_username` untuk memeriksa keberadaan pengguna yang diberi akses, dan terakhir fungsi `grant_userdb` untuk memberikan izin akses pengguna terhadap database yang diminta. Setelah izin akses diperoleh atau jika memang pengguna tersebut adalah pemilik database-nya, maka pengguna tersebut dapat menggunakan perintah `USE` untuk menggunakan suatu database yang ada (diperbolehkan). Berikut potongan kode jika permintaan (perintah) yang dimasukkan pengguna berupa perintah `USE`

```c
        else if(strstr(request, "USE")!=NULL){
            char dbname[1024];

            //extract dbname [TODO]
            extract_dbname_use(request, dbname);
            printf("DB %s\n", dbname);

            //check db's existance first, then user's permission.
            if(check_db(dbname)!=1){
                sprintf(response, "%s", "Database Does Not Exist");
            } else if(check_userdb(sender, dbname)!=1){
                printf("%s %s", sender, "user is not allowed to access the database");
                sprintf(response, "%s", "Permission Denied");
            } else {
                sprintf(response, "%s [%s]", "Database Used : ", dbname);
            }

        }
```
Pada potongan kode tersebut, terdapat percabangan yang memeriksa apakah *request* pengguna terdapat perintah `USE`. Dengan begitu, pengguna dapat menggunakan database yang diinginkan.

### Data Definition Lenguage

- CREATE DATABASE
```c
else if (strstr(request, "CREATE DATABASE") != NULL)
        {
            char dbname[1024];
            util_ddl_getdbname(request, dbname);

            int state = insert_db(dbname);
            if(state!=0){
                if(state==-1){
                    sprintf(response, "%s", "Failed to insert");
                }
                else if(state==-2){
                    sprintf(response, "%s", "Database Already Exists");
                }

                if (send(client_socket, response, strlen(response) + 1, 0) == -1){
                    perror("Send failed");
                }

                close(client_socket);
                continue;
            }

            
            int state_grant = grant_userdb(sender, dbname, 1);
            if(state_grant != 0){
                sprintf(response, "%s", "Failed to grant");
            }

            //admin could access all databases 
            if(strcmp(sender, "admin")!=0){
                if(grant_userdb("admin", dbname, 0)!=0){
                    sprintf(response, "%s", "Failed to grant admin or admin already exists");
                }
            }
            
            if(state==0 && state_grant==0){
                sprintf(response, "CREATE DATABASE : %s", dbname);
            }
        }
```

fungsi util_ddl_getdbname() : untuk mengextract string db.
```c
void util_ddl_getdbname(const char *req, char *dbname)
{
    const char *start = strstr(req, "CREATE DATABASE ") + strlen("CREATE DATABASE ");
    if (start == NULL)
    {
        perror("Invalid request format");
        exit(1);
    }

    const char *end = strchr(start, ';');
    if (end == NULL)
    {
        perror("Invalid request format");
        exit(1);
    }

    size_t nameLength = end - start;

    strncpy(dbname, start, nameLength);
    dbname[nameLength] = '\0';
}
```

fungsi insert_db : untuk menambahkan directory database ke path
```c
int insert_db(char *dbname)
{
    if(check_db(dbname)){
        return -2;
    }
    char cmd[1024];
    sprintf(cmd, "mkdir %s/%s", DATABASE_PATH, dbname);
    if(system(cmd)!=0){
        return -1;
    }

    char cmd2[1024];
    sprintf(cmd, "touch %s/%s/%s.txt", DATABASE_PATH, dbname, USERS_TABLE);
    if(system(cmd)!=0){
        return -1;
    }

    return 0;
}
```

fungsi grant_userdb :untuk grant user tersebut ke database yang baru dibuat
```c
int grant_userdb(char *user, char *db, int isfirst)
{
    char loc[1024];
    sprintf(loc, "%s/%s/%s.txt", DATABASE_PATH, db, USERS_TABLE);
    printf("loc gr %s\n", loc);

    if(check_userdb(user, db)){
        printf("User Already Granted\n");
        return -2;
    }

    if(isfirst) {
        //add header username|string
        FILE *fp = fopen(loc, "a");
        if (fp == NULL)
        {
            perror("Couldn't open the file grantuser");
            return -1;
        }

        char addeduser[256];
        snprintf(addeduser, sizeof(addeduser), "%s", "username string\n");
        fputs(addeduser, fp);
        fclose(fp);
    }

    FILE *fp = fopen(loc, "a");
    if (fp == NULL)
    {
        perror("Couldn't open the file grantuser");
        return -1;
    }

    char addeduser[256];
    snprintf(addeduser, sizeof(addeduser), "%s\n", user);
    fputs(addeduser, fp);
    fclose(fp);

    return 0;
}
```
- CREATE TABLE
```c
else if(strstr(request, "CREATE TABLE")!=NULL){
            char table_name[1024]; char colname[1024];char usedb[1024];
            extract_curdb(request, usedb);
            extract_table_columns(request, table_name, colname);

            //perform insertion of tables
            /*
                [assuming the request has a [db] passed into it] / [no need to check user's]
                [permission since it checks the existance of [db] param.                   ]
                1. create a txt file inside DATABASE_PATH/db/ with table_name as its name
                2. on the first line, write columns content :
                col1 int,col2 int,etc.
                3. end
            */
        
           //check if table already exists
            int tablestate, dbstate;
            tablestate = checktable(table_name, usedb);
            dbstate = strlen(usedb)<=0 ;
            
            if(dbstate==1){
                sprintf(response, "%s", "HINT : USE [db] First");
            }
            else if(checktable(table_name, usedb)!=1){
                char cmd[1024]; char loc[1024];
                strcpy(loc, DATABASE_PATH);
                strcat(loc, "/");
                strcat(loc, usedb);
                strcat(loc, "/");
                strcat(loc, table_name);
                strcat(loc, ".txt");

                //2 write cols data
                writeColumnsToFile(loc, colname);

                sprintf(response, "%s %s", "CREATE TABLE : ", table_name);
            } else {
                sprintf(response, "Table : %s Already Exists in Database %s", table_name, usedb);
            }
            
        }
```

fungsi extract_curdb : untuk mengekstrak string nama database dari request string
```c
void extract_curdb(const char* input_string, char* curdb) {
    int count = 0;
    const char* ptr = input_string;

    // Count the number of occurrences of '[]'
    while ((ptr = strchr(ptr, '[')) != NULL) {
        count++;
        ptr++;
    }
    
    if (count > 1) {
        const char* start = strrchr(input_string, '[');
        const char* end = strrchr(input_string, ']');

        if (start != NULL && end != NULL && end > start) {
            size_t length = end - start - 1;
            strncpy(curdb, start + 1, length);
            curdb[length] = '\0';
            return;
        }
    }

    strcpy(curdb, ""); 
}
```

fungsi extract_table_columns() : untuk mengestrak nama tabel dan kolomnya
```c
void extract_table_columns(const char* req, char* table_name, char* columns) {
    const char* start_table = strstr(req, "TABLE");
    const char* end_table = strchr(start_table, '(');
    const char* start_columns = end_table + 1;
    const char* end_columns = strchr(start_columns, ')');

    if (start_table != NULL && end_table != NULL && start_columns != NULL && end_columns != NULL && end_columns > start_columns) {
        size_t table_length = end_table - start_table - strlen("TABLE") - 1;
        strncpy(table_name, start_table + strlen("TABLE") + 1, table_length);
        table_name[table_length] = '\0';

        // Remove trailing whitespace from table_name
        while (table_length > 0 && isspace(table_name[table_length - 1])) {
            table_length--;
            table_name[table_length] = '\0';
        }

        size_t columns_length = end_columns - start_columns;
        strncpy(columns, start_columns, columns_length);
        columns[columns_length] = '\0';
    } else {
        strcpy(table_name, "");
        strcpy(columns, "");
    }
}
```

fungsi checktable()  : digunakan untuk cek apakah tabel tersebut sudah ada di dalam database
```c
int checktable(const char* tablename, const char* db) {
    char loc[1024];
    sprintf(loc, "%s/%s/", DATABASE_PATH, db);

    char filepath[1024];
    sprintf(filepath, "%s%s.txt", loc, tablename);

    if (access(filepath, F_OK) == 0) {
        return 1;  // File exists
    } else {
        return 0; // File does not exist or cannot be accessed
    }
}
```

- DROP DATABASE
```c
else if(strstr(request, "DROP DATABASE")!=NULL){
            //extract database name
            char dbname[1024];
            extract_name_dropdb(request, dbname);

            if (check_db(dbname)!=1){
                sprintf(response, "Coulnt Drop Database: %s Does Not Exist", dbname);
            }else if(check_userdb(sender, dbname)!=1){
                sprintf(response, "%s Permission Denied", sender);
            }
            else if(dropdb(dbname)!=0){
                sprintf(response, "Coulnt Drop %s Database", dbname);
            } else {
                sprintf(response, "Drop Successfull %s Database", dbname);
            }
        }
```

fungsi check_db() : digunakan untuk mengecek apakah database tersebut ada atau tidak
```c
int check_db(const char *db) {
    char loc[1024];
    sprintf(loc, "%s/%s", DATABASE_PATH, db);

    DIR *dir = opendir(loc);
    if (dir == NULL) {
        return 0;
    }

    // Directory exists
    closedir(dir);
    return 1;
}
```

- DROP TABLE
```c
 else if(strstr(request, "DROP TABLE")!=NULL){
            char table_name[1024];char usedb[1024];
            extract_curdb(request, usedb);
            extract_table(request, table_name);

            int dbstate = strlen(usedb)<=0;
            if(dbstate==1){
                sprintf(response, "%s", "HINT : USE [db] First");
            }
            else if(checktable(table_name, usedb)==1){
                char cmd[1024]; char loc[1024];
                strcpy(loc, DATABASE_PATH);
                strcat(loc, "/");
                strcat(loc, usedb);
                strcat(loc, "/");
                strcat(loc, table_name);
                strcat(loc, ".txt");

                sprintf(cmd, "rm %s", loc);
                system(cmd);

                sprintf(response, "%s %s", "DROP TABLE : ", table_name);
            } else {
                sprintf(response, "unknown Table : %s  in Database %s", table_name, usedb);
            }
        }
```

### Data Manipulation Language

fungsi `insert_data` digunakan untuk melakukan operasi insert data ke dalam sebuah tabel dalam database. Fungsi ini menerima tiga parameter: dbname yang merupakan nama database, table yang merupakan nama tabel, dan data yang merupakan data yang akan dimasukkan ke dalam tabel.
```c
// Fungsi untuk melakukan insert data ke tabel
int insert_data(const char* dbname, const char* table, const char* data)
{
    char filename[256];
    sprintf(filename, "%s/%s/%s.txt", DATABASE_PATH, dbname, table);

    FILE* fp = fopen(filename, "a");
    if (fp == NULL) {
        perror("Couldn't open the file for insert");
        return -1;
    }

    fputs(data, fp);
    fputs("\n", fp);

    fclose(fp);
    return 0;
}
```
##### Update

Fungsi `update_data` adalah sebuah fungsi yang digunakan untuk melakukan operasi update data pada sebuah tabel dalam database. Fungsi ini menerima tiga parameter: dbname yang merupakan nama database, table yang merupakan nama tabel, dan update_data yang merupakan data yang akan digunakan untuk melakukan update.
```c
// Fungsi untuk melakukan update data pada tabel
int update_data(const char* dbname, const char* table, const char* update_data)
{
    char filename[256];
    sprintf(filename, "%s/%s/%s.txt", DATABASE_PATH, dbname, table);

    FILE* fp = fopen(filename, "r");
    if (fp == NULL) {
        perror("Couldn't open the file for update");
        return -1;
    }

    char tmp_filename[256];
    sprintf(tmp_filename, "%s/%s/%s.tmp", DATABASE_PATH, dbname, table);

    FILE* tmp_fp = fopen(tmp_filename, "w");
    if (tmp_fp == NULL) {
        perror("Couldn't create temporary file for update");
        fclose(fp);
        return -1;
    }

    char line[256];
    int updated = 0;

    while (fgets(line, sizeof(line), fp) != NULL) {
        // Ubah baris data yang memenuhi kondisi
        if (check_condition(line, update_data)) {
            fputs(update_data, tmp_fp);
            fputs("\n", tmp_fp);
            updated = 1;
        } else {
            fputs(line, tmp_fp);
        }
    }

    fclose(fp);
    fclose(tmp_fp);

    if (!updated) {
        remove(tmp_filename);
        return -2; // Data not found
    }

    if (rename(tmp_filename, filename) != 0) {
        perror("Failed to rename temporary file");
        return -1;
    }

    return 0;
}

```
##### Delete

Fungsi `delete_data` adalah sebuah fungsi yang digunakan untuk melakukan operasi delete data dari sebuah tabel dalam database. Fungsi ini menerima dua parameter: dbname yang merupakan nama database, dan table yang merupakan nama tabel.
```c
// Fungsi untuk melakukan delete data dari tabel
int delete_data(const char* dbname, const char* table)
{
    char filename[256];
    sprintf(filename, "%s/%s/%s.txt", DATABASE_PATH, dbname, table);

    FILE* fp = fopen(filename, "r");
    if (fp == NULL) {
        perror("Couldn't open the file for delete");
        return -1;
    }

    char tmp_filename[256];
    sprintf(tmp_filename, "%s/%s/%s.tmp", DATABASE_PATH, dbname, table);

    FILE* tmp_fp = fopen(tmp_filename, "w");
    if (tmp_fp == NULL) {
        perror("Couldn't create temporary file for delete");
        fclose(fp);
        return -1;
    }

    char line[256];
    int deleted = 0;

    while (fgets(line, sizeof(line), fp) != NULL) {
        // Tidak menulis baris yang sesuai dengan kondisi ke file temporary
        // Baris tersebut dianggap terhapus
        deleted = 1;
    }

    fclose(fp);
    fclose(tmp_fp);

    if (!deleted) {
        remove(tmp_filename);
        return -2; // Data not found
    }

    if (rename(tmp_filename, filename) != 0) {
        perror("Failed to rename temporary file");
        return -1;
    }

    return 0;
}
```
##### Select

Fungsi `select_data`  digunakan untuk melakukan operasi select pada tabel dalam database. Fungsi ini menerima dua parameter: dbname yang merupakan nama database, dan request yang berisi permintaan select.
```c
void select_data(const char* dbname, const char* request) {
    char table_name[100];
    char columns[100];
    extract_table_columns(request, table_name, columns);

    // Daftar kolom yang tersedia
    const char* available_columns[] = {"kolom1", "kolom2", "kolom3"};
    int num_available_columns = sizeof(available_columns) / sizeof(available_columns[0]);

    char filename[256];
    sprintf(filename, "%s/%s/%s.txt", DATABASE_PATH, dbname, table_name);

    FILE* fp = fopen(filename, "r");
    if (fp == NULL) {
        perror("Couldn't open the file for select");
        return;
    }

    char line[256];
    int selected = 0;

    if (strcmp(columns, "*") == 0) {
        while (fgets(line, sizeof(line), fp) != NULL) {
            printf("%s", line);
            selected = 1;
        }
    } else {
        char* token;
        char* rest = columns;
        while ((token = strtok_r(rest, ",", &rest))) {
            int col_index = get_column_index(token, available_columns, num_available_columns);
            if (col_index != -1) {
                rewind(fp);
                while (fgets(line, sizeof(line), fp) != NULL) {
                    char* value = get_column_value(line, col_index);
                    printf("%s\t", value);
                    free(value);
                }
                printf("\n");
                selected = 1;
                rewind(fp);
            } else {
                printf("Column '%s' not found.\n", token);
            }
        }
    }

    fclose(fp);

    if (!selected) {
        printf("No data found.\n");
    }
}
```
##### check_condition

Fungsi `check_condition` digunakan sebagai langkah awal untuk melakukan pencarian dengan menggunakan klausa WHERE pada suatu baris data. Fungsi ini memeriksa apakah nilai pada kolom yang diberikan memenuhi kondisi yang ditentukan dalam format "<column>=<value>;". Jika kondisi cocok, maka fungsi mengembalikan nilai 1, dan jika kondisi tidak cocok, fungsi mengembalikan nilai 0.
```c
// Fungsi untuk memeriksa kondisi pada baris data
int check_condition(const char* data, const char* condition)
{
    char column[256];
    char value[256];
    sscanf(condition, "%[^=]=%[^;];", column, value);

    char* ptr = strstr(data, column);
    if (ptr == NULL) {
        return 0; // Column not found
    }

    ptr = strchr(ptr, '=') + 1;
    if (ptr == NULL) {
        return 0; // Invalid data format
    }

    if (strcmp(ptr, value) == 0) {
        return 1; // Condition matched
    } else {
        return 0; // Condition not matched
    }
}
```

### Loging
Fungsi log_command adalah sebuah fungsi dalam bahasa pemrograman C yang digunakan untuk mencatat log perintah yang dilakukan oleh pengguna ke dalam file `"log.txt"`. Fungsi ini menerima dua parameter: username yang merupakan nama pengguna yang melakukan perintah, dan command yang merupakan perintah yang dilakukan.
Fungsi ini mendapatkan waktu saat ini menggunakan fungsi `time` dan mengonversinya menjadi waktu lokal menggunakan fungsi `localtime`.
```c
void log_command(const char* username, const char* command) {
    time_t current_time;
    struct tm* time_info;
    char timestamp[20];

    // Dapatkan waktu saat ini
    time(&current_time);
    time_info = localtime(&current_time);

    // Format timestamp sesuai dengan format yang diinginkan
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", time_info);

    // Buka file log dalam mode append
    FILE* log_file = fopen("log.txt", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        return;
    }

    // Tulis log ke file
    fprintf(log_file, "%s:%s:%s\n", timestamp, username, command);

    // Tutup file log
    fclose(log_file);
}
```

### Error Handling
Apabila command yang tidak sesuai penggunaannya. Maka akan mengeluarkan pesan error tanpa keluar dari program client.
contoh penggunaan :
```c
    if (dbnameEnd == NULL) {
        perror("Invalid request format: missing 'INTO'");
        exit(1);
    }
```

### Reliability

### Containerization

### Revisi
Melakukan revisi pada point
- DML
- + Menambahkan fungsi : 
- + - insert_data
- + - update_data
- + - delete_data
- + - select_data
- + - check_condition

- Logging
- + Membuat log_command untuk menyimpan username, command dan timestamp yang didimpan kedalam log.txt

- Reliability

