#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>

#define SERVER_PORT 8888
#define USER_PATH "/home/fauzan/Desktop/fp/database/dbf02/users/datauser.txt"
#define DATABASE_PATH "/home/fauzan/Desktop/fp/database/dbf02/databases"
#define USERS_TABLE "USERS"

void daemonize();
void extractUsernameAndPassword(const char *, char *, char *, int);
void get_sender(const char *, char *, size_t);
int add_user(char *, char *);
int check_user(char *, char *);

void extractUsernameAndPassword(const char *request, char *username, char *password, int mode)
{
    // Find the position of the username and password in the request string
    const char *usernameStart;
    const char *passwordStart;
    if (mode == 1)
    {
        usernameStart = strstr(request, "CREATE USER ") + strlen("CREATE USER ");
        passwordStart = strstr(request, "IDENTIFIED BY ") + strlen("IDENTIFIED BY ");
    }
    else if (mode == 2)
    {
        usernameStart = strstr(request, "LOGIN USER ") + strlen("LOGIN USER ");
        passwordStart = strstr(request, "IDENTIFIED BY ") + strlen("IDENTIFIED BY ");
    }

    // Find the end position of the username and password
    const char *usernameEnd = strchr(usernameStart, ' ');
    const char *passwordEnd = strchr(passwordStart, ';');

    // Calculate the lengths of the username and password
    size_t usernameLength = usernameEnd - usernameStart;
    size_t passwordLength = passwordEnd - passwordStart;

    // Copy the username and password to the allocated buffers
    strncpy(username, usernameStart, usernameLength);
    username[usernameLength] = '\0';

    strncpy(password, passwordStart, passwordLength);
    password[passwordLength] = '\0';
}

void daemonize() //not tested
{
    pid_t pid = fork();

    if (pid < 0)
    {
        perror("Fork failed");
        exit(1);
    }

    if (pid > 0)
    {
        exit(0); // Parent process exits
    }

    // Child process continues
    umask(0); // Set file permissions for the daemon

    if (setsid() < 0)
    {
        perror("Failed to set process group ID");
        exit(1);
    }

    // Redirect standard input/output/error to null
    int null_fd = open("/dev/null", O_RDWR);
    if (null_fd < 0)
    {
        perror("Failed to open /dev/null");
        exit(1);
    }
    dup2(null_fd, STDIN_FILENO);
    dup2(null_fd, STDOUT_FILENO);
    dup2(null_fd, STDERR_FILENO);
    close(null_fd);
}

int check_username(char *user)
{
    FILE *fp = fopen(USER_PATH, "r");
    if (fp == NULL)
    {
        perror("Couldn't open the file checkuser");
        return -1;
    }

    char line[256];
    int found = 0;

    while (fgets(line, sizeof(line), fp)!=NULL)
    {
        char stored_user[256];
        char stored_pw[256];
        sscanf(line, "%[^,],%s", stored_user, stored_pw);

        if (strcmp(user, stored_user) == 0)
        {
            found = 1;
            break;
        }
    }

    fclose(fp);

    return found;
}

int check_user(char *user, char *pw)
{
    FILE *fp = fopen(USER_PATH, "r");
    if (fp == NULL)
    {
        perror("Couldn't open the file checkuser");
        return -1;
    }

    char line[256];
    int found = 0;

    while (fgets(line, sizeof(line), fp)!=NULL)
    {
        char stored_user[256];
        char stored_pw[256];
        sscanf(line, "%[^,],%s", stored_user, stored_pw);

        if (strcmp(user, stored_user) == 0)
        {
            if (strcmp(pw, stored_pw) == 0)
            {
                found = 2;
                break;
            }

            found = 1;
            break;
        }
    }

    fclose(fp);

    return found;
}

int add_user(char *user, char *pw)
{
    if (check_user(user, pw) != 0)
    {
        perror("User Already Existed");
        return -2;
    }

    FILE *fp = fopen(USER_PATH, "a");
    if (fp == NULL)
    {
        perror("Couldn't open the file adduser");
        return -1;
    }

    fseek(fp, 0, SEEK_END);  // Move the file pointer to the end of the file
    long file_size = ftell(fp);  // Get the current position of the file pointer

    int is_empty = (file_size == 0);
    if(is_empty){
        fprintf(fp, "username string,password string\n");
    }

    char user_pw[256];
    snprintf(user_pw, sizeof(user_pw), "%s,%s\n", user, pw);
    fputs(user_pw, fp);
    fclose(fp);

    return 0;
}


void get_sender(const char *req, char *name, size_t maxLength)
{
    const char *start = strchr(req, '[');
    if (start == NULL)
    {
        perror("Invalid request format: No opening square bracket found");
        exit(1);
    }
    start++; // Move the start pointer past the opening square bracket

    const char *end = strchr(start, ']');
    if (end == NULL)
    {
        perror("Invalid request format: No closing square bracket found");
        exit(1);
    }

    size_t nameLength = end - start;
    if (nameLength >= maxLength) {
        perror("Username exceeds maximum length");
        exit(1);
    }

    strncpy(name, start, nameLength);
    name[nameLength] = '\0';
}


void util_ddl_getdbname(const char *req, char *dbname)
{
    const char *start = strstr(req, "CREATE DATABASE ") + strlen("CREATE DATABASE ");
    if (start == NULL)
    {
        perror("Invalid request format");
        exit(1);
    }

    const char *end = strchr(start, ';');
    if (end == NULL)
    {
        perror("Invalid request format");
        exit(1);
    }

    size_t nameLength = end - start;

    strncpy(dbname, start, nameLength);
    dbname[nameLength] = '\0';
}


int check_db(const char *db) {
    char loc[1024];
    sprintf(loc, "%s/%s", DATABASE_PATH, db);

    DIR *dir = opendir(loc);
    if (dir == NULL) {
        return 0;
    }

    // Directory exists
    closedir(dir);
    return 1;
}

int insert_db(char *dbname)
{
    if(check_db(dbname)){
        return -2;
    }
    char cmd[1024];
    sprintf(cmd, "mkdir %s/%s", DATABASE_PATH, dbname);
    if(system(cmd)!=0){
        return -1;
    }

    char cmd2[1024];
    sprintf(cmd, "touch %s/%s/%s.txt", DATABASE_PATH, dbname, USERS_TABLE);
    if(system(cmd)!=0){
        return -1;
    }

    return 0;
}


int check_userdb(char *user, char *db){
    char loc[1024];
    sprintf(loc, "%s/%s/%s.txt", DATABASE_PATH, db, USERS_TABLE);
    printf("loc %s\n", loc);

    FILE *fp = fopen(loc, "r");
    if (fp == NULL)
    {
        perror("Couldn't open the file checkuserdb");
        return -1;
    }

    char line[256];
    int found = 0;

    int linenum = 0;
    while (fgets(line, sizeof(line), fp) != NULL)
    {
        linenum++;
        if (linenum == 1)
        {
            continue;  // Skip line 1
        }

        // Remove the newline character from the end of the line
        size_t len = strlen(line);
        if (len > 0 && line[len - 1] == '\n')
        {
            line[len - 1] = '\0';
        }

        char stored_user[256];
        char stored_pw[256];
        sscanf(line, "%[^,],%s", stored_user, stored_pw);
        printf("%s %s\n", user, stored_user);
        if (strcmp(user, stored_user) == 0)
        {
            found = 1;
            break;
        }
    }


    fclose(fp);

    return found;
}

int grant_userdb(char *user, char *db, int isfirst)
{
    char loc[1024];
    sprintf(loc, "%s/%s/%s.txt", DATABASE_PATH, db, USERS_TABLE);
    printf("loc gr %s\n", loc);

    if(check_userdb(user, db)){
        printf("User Already Granted\n");
        return -2;
    }

    if(isfirst) {
        //add header username|string
        FILE *fp = fopen(loc, "a");
        if (fp == NULL)
        {
            perror("Couldn't open the file grantuser");
            return -1;
        }

        char addeduser[256];
        snprintf(addeduser, sizeof(addeduser), "%s", "username string\n");
        fputs(addeduser, fp);
        fclose(fp);
    }

    FILE *fp = fopen(loc, "a");
    if (fp == NULL)
    {
        perror("Couldn't open the file grantuser");
        return -1;
    }

    char addeduser[256];
    snprintf(addeduser, sizeof(addeduser), "%s\n", user);
    fputs(addeduser, fp);
    fclose(fp);

    return 0;
}


void auth_getdb(const char *req, char *dbname, char *usrname) {
    const char* grantStart = strstr(req, "GRANT PERMISSION ");
    if (grantStart == NULL) {
        perror("Invalid request format: missing 'GRANT PERMISSION'");
        exit(1);
    }

    const char* dbnameStart = grantStart + strlen("GRANT PERMISSION ");
    const char* dbnameEnd = strchr(dbnameStart, ' ');
    if (dbnameEnd == NULL) {
        perror("Invalid request format: missing database name");
        exit(1);
    }

    const char* intoStart = strstr(dbnameEnd + 1, "INTO ");
    if (intoStart == NULL) {
        perror("Invalid request format: missing 'INTO'");
        exit(1);
    }

    const char* usernameStart = intoStart + strlen("INTO ");
    const char* usernameEnd = strchr(usernameStart, ';');
    if (usernameEnd == NULL) {
        perror("Invalid request format: missing semicolon");
        exit(1);
    }

    size_t dbnameLength = dbnameEnd - dbnameStart;
    size_t usernameLength = usernameEnd - usernameStart;

    if (dbnameLength >= 1024 || usernameLength >= 1024) {
        perror("Destination buffer too small");
        exit(1);
    }

    strncpy(usrname, dbnameStart, dbnameLength);
    usrname[dbnameLength] = '\0';

    strncpy(dbname, usernameStart, usernameLength);
    dbname[usernameLength] = '\0';
}

int dropdb(char *dbname){
    char cmd[1024];
    char pathdb[1024];
    sprintf(pathdb, "%s/%s/", DATABASE_PATH, dbname);
    sprintf(cmd, "rm -r %s", pathdb);

    if(system(cmd)!=0){
        perror("Drop DB Error");
        return -1;
    }

    return 0;
}


void extract_name_dropdb(char *req, char *extracted)
{
    const char *start = strstr(req, "DROP DATABASE "); 
    if (start == NULL)
    {
        strcpy(extracted, ""); 
        printf("Name extraction failed. 'DROP DATABASE ' not found.\n");
        return;
    }

    start += strlen("DROP DATABASE ");  

    const char *end = strchr(start, ';');  
    if (end == NULL)
    {
        strcpy(extracted, "");  
        printf("Name extraction failed. ';' not found.\n");
        return;
    }
    size_t length = end - start;

    strncpy(extracted, start, length); 
    extracted[length] = '\0';  
}

void extract_dbname_use(char* input_string, char* dbname) {
    char *start = strstr(input_string, "USE");
    if (start != NULL) {
        start += strlen("USE") + 1; // Skip "USE" and the following space
        sscanf(start, "%s", dbname);
        dbname[strlen(dbname)-1] = '\0';
    } else {
        strcpy(dbname, ""); // Empty string if "USE" keyword is not found
    }
}

//mantap
void extract_curdb(const char* input_string, char* curdb) {
    int count = 0;
    const char* ptr = input_string;

    // Count the number of occurrences of '[]'
    while ((ptr = strchr(ptr, '[')) != NULL) {
        count++;
        ptr++;
    }
    
    if (count > 1) {
        const char* start = strrchr(input_string, '[');
        const char* end = strrchr(input_string, ']');

        if (start != NULL && end != NULL && end > start) {
            size_t length = end - start - 1;
            strncpy(curdb, start + 1, length);
            curdb[length] = '\0';
            return;
        }
    }

    strcpy(curdb, ""); 
}


void extract_table_columns(const char* req, char* table_name, char* columns) {
    const char* start_table = strstr(req, "TABLE");
    const char* end_table = strchr(start_table, '(');
    const char* start_columns = end_table + 1;
    const char* end_columns = strchr(start_columns, ')');

    if (start_table != NULL && end_table != NULL && start_columns != NULL && end_columns != NULL && end_columns > start_columns) {
        size_t table_length = end_table - start_table - strlen("TABLE") - 1;
        strncpy(table_name, start_table + strlen("TABLE") + 1, table_length);
        table_name[table_length] = '\0';

        // Remove trailing whitespace from table_name
        while (table_length > 0 && isspace(table_name[table_length - 1])) {
            table_length--;
            table_name[table_length] = '\0';
        }

        size_t columns_length = end_columns - start_columns;
        strncpy(columns, start_columns, columns_length);
        columns[columns_length] = '\0';
    } else {
        strcpy(table_name, "");
        strcpy(columns, "");
    }
}

void extract_table(const char* req, char* table_name) {
    const char* start_table = strstr(req, "TABLE ");
    const char* end_table = strchr(start_table, ';');

    if (start_table != NULL && end_table != NULL) {
        size_t table_length = end_table - start_table - strlen("TABLE") - 1;
        strncpy(table_name, start_table + strlen("TABLE") + 1, table_length);
        table_name[table_length] = '\0';

        // Remove trailing whitespace from table_name
        while (table_length > 0 && isspace(table_name[table_length - 1])) {
            table_length--;
            table_name[table_length] = '\0';
        }

    } else {
        strcpy(table_name, "");
    }
}

void writeColumnsToFile(const char* filename, const char* columns) {
    FILE* file = fopen(filename, "w");
    if (file != NULL) {
        fprintf(file, "%s\n", columns);
        fclose(file);
        printf("Columns written to file: %s\n", filename);
    } else {
        printf("Failed to open file: %s\n", filename);
    }
}

int checktable(const char* tablename, const char* db) {
    char loc[1024];
    sprintf(loc, "%s/%s/", DATABASE_PATH, db);

    char filepath[1024];
    sprintf(filepath, "%s%s.txt", loc, tablename);

    if (access(filepath, F_OK) == 0) {
        return 1;  // File exists
    } else {
        return 0; // File does not exist or cannot be accessed
    }
}


int validate(const char *buffer) {
    char tmp[512];
    strncpy(tmp, buffer, 511);  // Menggunakan strncpy untuk menghindari buffer overflow
    tmp[511] = '\0';  // Menyimpan null-terminator di akhir string tmp
    char g = '\'';

    if (tmp[0] == g && tmp[strlen(tmp) - 1] == g) {
        return 1;
    }

    for (int i = 0; i < strlen(tmp); i++) {
        if (!isdigit(tmp[i])) {
            return 0;
        }
    }

    return 2;
}

//Command
void util_dml_insert(const char *req, char *tableName, char *values) {
    const char *start = strstr(req, "INSERT INTO ") + strlen("INSERT INTO ");
    if (start == NULL) {
        perror("Invalid request format");
        exit(1);
    }

    const char *end = strchr(start, '(');
    if (end == NULL) {
        perror("Invalid request format");
        exit(1);
    }

    size_t nameLength = end - start;

    strncpy(tableName, start, nameLength);
    tableName[nameLength] = '\0';

    start = end + 1;
    end = strchr(start, ')');
    if (end == NULL) {
        perror("Invalid request format");
        exit(1);
    }

    nameLength = end - start;

    strncpy(values, start, nameLength);
    values[nameLength] = '\0';
}

//INSERT
int insertTable(const char *buffer, char *useDatabase) {
    if (useDatabase == NULL || useDatabase[0] == '\0') {
        return -1;
    }

    char tmp[512];
    strncpy(tmp, buffer, sizeof(tmp) - 1);
    tmp[sizeof(tmp) - 1] = '\0';

    char *token = strtok(tmp, "(");
    token = strtok(NULL, "(");
    token = strtok(token, ")");
    token = strtok(token, ",");

    char data[100][512];
    int i = 0;

    while (token != NULL) {
        strcpy(data[i], trim(token));
        i++;
        token = strtok(NULL, ",");
    }

    FILE *fileIn, *fileout;

    strncpy(tmp, buffer, sizeof(tmp) - 1);
    tmp[sizeof(tmp) - 1] = '\0';

    token = strtok(tmp, " ");
    token = strtok(NULL, " ");
    token = strtok(NULL, " ");

    char open[512] = {0}, append[512] = {0};
    snprintf(open, sizeof(open), "%s/databases/%s/struktur_%s.txt", DATABASE_PATH, useDatabase, token);
    fileIn = fopen(open, "r");

    char data_type[100][512];
    char tmp_type[512], ret[512];
    int k = 0;

    if (fileIn) {
        while (fscanf(fileIn, "%s %s", ret, tmp_type) != EOF) {
            strcpy(data_type[k], tmp_type);
            k++;
        }
    } else {
        return -2;
    }

    snprintf(append, sizeof(append), "%s/databases/%s/%s.txt", DATABASE_PATH, useDatabase, token);
    fileout = fopen(append, "a");

    if (k != i) {
        fclose(fileIn);
        fclose(fileout);
        return -3;
    }

    for (int j = 0; j < i; j++) {
        int val = validate(data[j]);
        if ((val == 1 && strcmp(data_type[j], "string") == 0) || (val == 2 && strcmp(data_type[j], "int") == 0)) {
            continue;
        } else {
            fclose(fileIn);
            fclose(fileout);
            return -4;
        }
    }

    for (int j = 0; j < i - 1; j++) {
        fprintf(fileout, "%s,", data[j]);
    }
    fprintf(fileout, "%s\n", data[i - 1]);

    fclose(fileIn);
    fclose(fileout);

    return 1;
}

//UPDATE
int update(const char *buffer, char *useDatabase) {
    if (strlen(useDatabase) == 0) {
        return -1;
    }

    char tempBuffer[1024] = {0};
    strcpy(tempBuffer, buffer);

    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer, " ");
    char input[10][512];
    int i = 0;

    while (token != NULL) {
        strcpy(input[i++], token);
        token = strtok(NULL, " ");
    }

    if (strcmp(input[2], "SET") != 0) {
        return -2;
    }

    char tableName[512], columnName[512], value[512];

    token = strtok(input[1], "=");
    strcpy(tableName, token);
    token = strtok(NULL, "=");
    strcpy(columnName, token);

    token = strtok(input[3], "=");
    strcpy(value, token);

    char path[10000], nPath[10000];

    sprintf(path, "%s/databases/%s/%s.txt", DATABASE_PATH, useDatabase, tableName);
    sprintf(nPath, "%s/databases/%s/%s2.txt", DATABASE_PATH, useDatabase, tableName);
    FILE *fileIn, *fileout;
    fileIn = fopen(path, "r");
    fileout = fopen(nPath, "w");
    char getVal[512], newVal[512];
    if (fileIn) {
        while (fgets(getVal, 512, fileIn)) {
            strcpy(newVal, getVal);
            token = strtok(newVal, ",");
            int j = 0;
            while (token != NULL) {
                if (j == 0 && strcmp(columnName, token) == 0) {
                    fprintf(fileout, "%s,", value);
                } else {
                    fprintf(fileout, "%s,", token);
                }
                token = strtok(NULL, ",");
                j++;
            }
            newVal[strlen(newVal) - 1] = '\0';
            fprintf(fileout, "\n");
        }
        fclose(fileIn);
        fclose(fileout);
        remove(path);
        rename(nPath, path);
        return 1;
    }
    return -1;
}


int main()
{
    // Create a daemon process
    // Create a socket
    int server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket == -1)
    {
        perror("Socket creation failed");
        return 1;
    }

    int reuse = 1;
    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) == -1)
    {
        perror("Setsockopt failed");
        return 1;
    }

    // Bind the socket to an IP address and port
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(SERVER_PORT);
    server_address.sin_addr.s_addr = INADDR_ANY;
    if (bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address)) == -1)
    {
        perror("Socket binding failed");
        return 1;
    }

    // Listen for incoming connections
    if (listen(server_socket, 5) == -1)
    {
        perror("Listen failed");
        return 1;
    }

    printf("Server is listening on port %d\n", SERVER_PORT);

    while (1)
    {
        // Accept a client connection
        struct sockaddr_in client_address;
        socklen_t client_address_length = sizeof(client_address);
        int client_socket = accept(server_socket, (struct sockaddr *)&client_address, &client_address_length);
        if (client_socket == -1)
        {
            perror("Accept failed");
            continue;
        }

        printf("Client connected\n");

        // Receive request from the client
        char request[1024];
        ssize_t request_length = recv(client_socket, request, sizeof(request), 0);
        if (request_length == -1)
        {
            perror("Receive failed");
            close(client_socket);
            continue;
        }

        char response[1024];

        // Process the request
        // In this example, we simply print the request and send a generic response back to the client
        printf("Received request: %.*s\n", (int)request_length, request);

        char sender[1024];
        get_sender(request, sender, sizeof(sender));
        printf("sender : %s\n", sender);

        if (strstr(request, "CREATE USER") != NULL)
        {
            char usrname[1024], password[1024];
            extractUsernameAndPassword(request, usrname, password, 1);
    

            int is_admin = strcmp(sender, "admin") == 0;
            if (!is_admin)
            {
                printf("Create User Info : %s access denied\n", sender);
                sprintf(response, "%s", "non-admin can not create a user. ");
            }

            int state = add_user(usrname, password);
            if (state != 0 && is_admin){
                perror("SERVER : Failed to Create User");
                if (state == -1){
                    sprintf(response, "%s", "Failed to Create User.");
                }
                else if (state == -2){
                    sprintf(response, "%s", "Failed to Create User: user already existed");
                }
            }
            else if (state == 0 && is_admin)
            {
                sprintf(response, "%s", "User Created.");
            }
        }

        else if (strstr(request, "LOGIN USER") != NULL)
        {
            char usrname[1024], password[1024];
            extractUsernameAndPassword(request, usrname, password, 2);

            int state = check_user(usrname, password);
            if (state == 2)
            {
                printf("LOGGED IN: %s\n", usrname);
                sprintf(response, "%s %s", "LOGGED IN :", usrname);
            }
            else
            {
                printf("LOGIN FAILED : %s\n", usrname);
                sprintf(response, "%s", "Wrong Password or Username ");
            }
        }

        else if (strstr(request, "CREATE DATABASE") != NULL)
        {
            char dbname[1024];
            util_ddl_getdbname(request, dbname);

            int state = insert_db(dbname);
            if(state!=0){
                if(state==-1){
                    sprintf(response, "%s", "Failed to insert");
                }
                else if(state==-2){
                    sprintf(response, "%s", "Database Already Exists");
                }

                if (send(client_socket, response, strlen(response) + 1, 0) == -1){
                    perror("Send failed");
                }

                close(client_socket);
                continue;
            }

            
            int state_grant = grant_userdb(sender, dbname, 1);
            if(state_grant != 0){
                sprintf(response, "%s", "Failed to grant");
            }

            //admin could access all databases 
            if(strcmp(sender, "admin")!=0){
                if(grant_userdb("admin", dbname, 0)!=0){
                    sprintf(response, "%s", "Failed to grant admin or admin already exists");
                }
            }
            
            if(state==0 && state_grant==0){
                sprintf(response, "CREATE DATABASE : %s", dbname);
            }
        }


        else if(strstr(request, "DROP DATABASE")!=NULL){
            //extract database name
            char dbname[1024];
            extract_name_dropdb(request, dbname);

            if (check_db(dbname)!=1){
                sprintf(response, "Coulnt Drop Database: %s Does Not Exist", dbname);
            }else if(check_userdb(sender, dbname)!=1){
                sprintf(response, "%s Permission Denied", sender);
            }
            else if(dropdb(dbname)!=0){
                sprintf(response, "Coulnt Drop %s Database", dbname);
            } else {
                sprintf(response, "Drop Successfull %s Database", dbname);
            }
        } 

        else if(strstr(request, "CREATE TABLE")!=NULL){
            char table_name[1024]; char colname[1024];char usedb[1024];
            extract_curdb(request, usedb);
            extract_table_columns(request, table_name, colname);

            //perform insertion of tables
            /*
                [assuming the request has a [db] passed into it] / [no need to check user's]
                [permission since it checks the existance of [db] param.                   ]
                1. create a txt file inside DATABASE_PATH/db/ with table_name as its name
                2. on the first line, write columns content :
                col1 int,col2 int,etc.
                3. end
            */
        
           //check if table already exists
            int tablestate, dbstate;
            tablestate = checktable(table_name, usedb);
            dbstate = strlen(usedb)<=0 ;
            
            if(dbstate==1){
                sprintf(response, "%s", "HINT : USE [db] First");
            }
            else if(checktable(table_name, usedb)!=1){
                char cmd[1024]; char loc[1024];
                strcpy(loc, DATABASE_PATH);
                strcat(loc, "/");
                strcat(loc, usedb);
                strcat(loc, "/");
                strcat(loc, table_name);
                strcat(loc, ".txt");

                //2 write cols data
                writeColumnsToFile(loc, colname);

                sprintf(response, "%s %s", "CREATE TABLE : ", table_name);
            } else {
                sprintf(response, "Table : %s Already Exists in Database %s", table_name, usedb);
            }
            
        }

        else if(strstr(request, "DROP TABLE")!=NULL){
            char table_name[1024];char usedb[1024];
            extract_curdb(request, usedb);
            extract_table(request, table_name);

            int dbstate = strlen(usedb)<=0;
            if(dbstate==1){
                sprintf(response, "%s", "HINT : USE [db] First");
            }
            else if(checktable(table_name, usedb)==1){
                char cmd[1024]; char loc[1024];
                strcpy(loc, DATABASE_PATH);
                strcat(loc, "/");
                strcat(loc, usedb);
                strcat(loc, "/");
                strcat(loc, table_name);
                strcat(loc, ".txt");

                sprintf(cmd, "rm %s", loc);
                system(cmd);

                sprintf(response, "%s %s", "DROP TABLE : ", table_name);
            } else {
                sprintf(response, "unknown Table : %s  in Database %s", table_name, usedb);
            }
        }

        // GRANT PERMISSION -> auth sub.1
        //TODO : sebelum grant user harus cari dulu, si granted user ini user apa ngga -> check dbf02/users/datauser.txt
        // done kayaknya
        else if(strstr(request, "GRANT PERMISSION")!=NULL){
            int is_admin = strcmp(sender, "admin")==0;

            if(is_admin){
                char dbname[1024];
                char granted_user[1024];
                auth_getdb(request, dbname, granted_user);
                printf("nama %s db %s\n", granted_user, dbname);

                //cek db's existance
                if(check_db(dbname)!=1){
                    printf("DB: %s does not exist\n", dbname);
                    sprintf(response, "%s", "Database Does not exist");
                }

                //check user existance
                else if(check_username(granted_user)){
                    int state = grant_userdb(granted_user, dbname, 0);
                    if(state!=0){
                        if(state == -2){
                            sprintf(response, "%s", "User Already Granted on Database");
                        }else{
                            sprintf(response, "%s", "Failed to grant user");
                        }
                    }else{
                        sprintf(response, "%s", "Successfully Granted the User");
                    }
                } else {
                    sprintf(response, "%s %s", granted_user, "is unregistered as a user");
                }                
            } else{
                sprintf(response, "%s", "Non-admin can not grant other user's permission");
            }
        }

        else if(strstr(request, "USE")!=NULL){
            char dbname[1024];

            //extract dbname [TODO]
            extract_dbname_use(request, dbname);
            printf("DB %s\n", dbname);

            //check db's existance first, then user's permission.
            if(check_db(dbname)!=1){
                sprintf(response, "%s", "Database Does Not Exist");
            } else if(check_userdb(sender, dbname)!=1){
                printf("%s %s", sender, "user is not allowed to access the database");
                sprintf(response, "%s", "Permission Denied");
            } else {
                sprintf(response, "%s [%s]", "Database Used : ", dbname);
            }

        }

        else if (strstr(request, "quit") != NULL)
        {
            sprintf(response, "%s", "Client Session closed");
            printf("Client Disconnected\n");
        }

        else{
            sprintf(response, "%s", "No command found bro");
        }

        if (send(client_socket, response, strlen(response) + 1, 0) == -1)
        {
            perror("Send failed");
        }

        close(client_socket);
    }

    // Close the server socket
    close(server_socket);

    return 0;
}
