#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//USE mydb;
//->
//USE mydb; [arfi]

#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 8888

void print_usage();
void login_main();
void extractDatabaseName(char*, char* );

void extractDatabaseName(char* res, char* dbname) {
    const char* delimiter = ":";
    const char* token = strtok(res, delimiter);
    
    while (token != NULL) {
        token = strtok(NULL, delimiter);
        if (token != NULL) {
            strcpy(dbname, token);
            break;
        }
    }
}


void print_usage() {
    printf("Usage:\n");
    printf("To create a user (sudo):\n");
    printf("  sudo ./client\n");
    printf("To login (non-sudo):\n");
    printf("  ./client -u <username> -p <password>\n");
    printf("   username can not be \"admin\" \n");
}


int connect_me(){
    int client_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket == -1) {
        perror("Socket creation failed");
        return -1;
    }

    // Connect to the server
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(SERVER_PORT);
    if (inet_pton(AF_INET, SERVER_IP, &(server_address.sin_addr)) <= 0) {
        perror("Invalid server address");
        return -1;
    }
    if (connect(client_socket, (struct sockaddr*)&server_address, sizeof(server_address)) == -1) {
        perror("Connection failed");
        return -1;
    }

    return client_socket;
}


void login_main(int client_socket, char *clientname, int is_sudo) {
    char cmd[1024];
    char response[1024];

    char cur_database[1024];

    int i = 0;
    do {
        if(i!=0){
            client_socket = connect_me();
        }
        fgets(cmd, sizeof(cmd), stdin);
        cmd[strcspn(cmd, "\n")] = '\0'; 

        if (strcmp(cmd, "\n") == 0){
            continue;
        }

        char *request = malloc(strlen(clientname) + 1024);
        strcpy(request, cmd);
        strcat(request, " [");
        strcat(request, clientname);
        strcat(request, "]");

        if(strlen(cur_database)!=0){
            char tempstr[256];
            sprintf(tempstr, "%s", cur_database);
            strcat(request, tempstr);
        }

        // request format: [COMMAND] -u [CLIENTNAME]
        if (send(client_socket, request, strlen(request), 0) == -1) {
            perror("Send failed");
            break;
        }

        if (recv(client_socket, response, sizeof(response), 0) == -1) {
            perror("Receive failed");
            return;
        }

        printf("=> [Server Response] : %s\n", response);
        if(strcmp(response, "Client Session closed")==0){
            break;
        } else if(strstr(response, "Database Used")!=0){
            extractDatabaseName(response, cur_database);
        }
        free(request);
        i++;
    } while (1);
}



int main(int argc, char *argv[]) {
    int is_sudo = (geteuid() == 0);


    char* username = NULL;
    char* password = NULL;

    // Check if running as sudo or non-sudo
    if (is_sudo) {
        
        if (argc != 1) {
            print_usage();
            return 1;
        }

        
    } else {
        // Running as non-sudo, login with username and password
        int option;

        // Parse command line arguments
        while ((option = getopt(argc, argv, "u:p:")) != -1) {
            switch (option) {
                case 'u':
                    username = optarg;
                    break;
                case 'p':
                    password = optarg;
                    break;
                default:
                    print_usage();
                    return 1;
            }
        }

        // Validate command line arguments
        if (username == NULL || password == NULL || strcmp(username, "admin")==0) {
            print_usage();
            return 1;
        }
    }

    // Create a socket
    int client_socket = connect_me();
    if(client_socket==-1){
        return -1;
    }

    // Prepare the request string based on user type
    char request[1024];
    char response[1024];
    if (!is_sudo) {
        sprintf(request, "LOGIN USER %s IDENTIFIED BY %s; [%s]", username, password, username);

            // Send the request to the server
        if (send(client_socket, request, strlen(request), 0) == -1) {
            perror("Send failed");
            return 1;
        }

        // Receive and print the response from the server
        if (recv(client_socket, response, sizeof(response), 0) == -1) {
            perror("Receive failed");
            return 1;
        }
        printf("=> [Server Response] : %s\n", response);
    } 

    
    if(strstr(response, "LOGGED IN")!=NULL && !is_sudo){
        client_socket = connect_me();
        login_main(client_socket, username, is_sudo);
    } else if (is_sudo){
        login_main(client_socket, "admin", is_sudo);
    }

    // Close the socket
    close(client_socket);

    return 0;
}
