# Set the base image
FROM gcc:latest

# Set the working directory
WORKDIR /app

# Copy the source files to the container
COPY client/client.c /app/client/
COPY database/database.c /app/database/

# Build the client and database executables
RUN gcc -o client/client client/client.c
RUN gcc -o database/database database/database.c

# Set the entrypoint command to execute database first and then client
ENTRYPOINT ["sh", "-c", "./database/database && ./client/client"]
